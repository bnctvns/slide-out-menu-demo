//
//  HomeViewController.swift
//  Slide Out Menu Demo
//
//  Created by boni octavianus on 3/9/16.
//  Copyright © 2016 bonioctavianus. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var barButtonOpen: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        barButtonOpen.target = self.revealViewController()
        barButtonOpen.action = Selector("revealToggle:")
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
