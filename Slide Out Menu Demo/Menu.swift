//
//  Menu.swift
//  Slide Out Menu Demo
//
//  Created by boni octavianus on 3/10/16.
//  Copyright © 2016 bonioctavianus. All rights reserved.
//

import Foundation

class Menu {
    
    var name: String
    
    init(name: String) {
        self.name = name
        
    }
    
    class func getListSlideOutMenu() -> [Menu] {
        
        var listMenu = [Menu]()
        
        var menu = Menu(name: "Home")
        listMenu.append(menu)
        
        menu = Menu(name: "Account")
        listMenu.append(menu)
        
        menu = Menu(name: "About")
        listMenu.append(menu)
        
        return listMenu
    }
}