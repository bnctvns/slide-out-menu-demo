//
//  SlideOutMenuTableViewController.swift
//  Slide Out Menu Demo
//
//  Created by boni octavianus on 3/10/16.
//  Copyright © 2016 bonioctavianus. All rights reserved.
//

import UIKit



class SlideOutMenuTableViewController : UITableViewController {
    
    var listSlideOutMenu = [Menu]()
    
    override func viewDidLoad() {
        
        listSlideOutMenu = Menu.getListSlideOutMenu()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listSlideOutMenu.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(listSlideOutMenu[indexPath.row].name, forIndexPath: indexPath) as UITableViewCell
        
        cell.textLabel!.text = listSlideOutMenu[indexPath.row].name
        
        return cell
    }
}